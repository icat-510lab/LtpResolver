package sdu.ltp.web.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

@ControllerAdvice(basePackages="sdu.icat.web.controller")
public class JsonAdvice extends AbstractJsonpResponseBodyAdvice {

    public JsonAdvice() {
	super("callback","jsonp");
    }
    
}
