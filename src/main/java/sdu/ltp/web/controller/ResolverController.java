package sdu.ltp.web.controller;

import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import sdu.ltp.entity.DataBaseFascade;
import sdu.ltp.entity.StyleMapper;
import sdu.ltp.resolve.ResolverArticleExecutor;
import sdu.ltp.resolve.ResolverSampleExecutor;

/**
 * 定义解析的web接口
 * @author ljh_2015
 *
 */
@RestController
@Api(value="resolver")
public class ResolverController {

    @GetMapping("/resolve")
    @ApiOperation(value="解析")
    public List<StyleMapper> getMaps(String text) {
	return ResolverSampleExecutor.simpleExecutor(text);
    }
    
    @GetMapping("/arraykey")
    public Map<String,Integer> getAllKeys() {
	return DataBaseFascade.newInstance().getAllkeys();
    }
    
    @GetMapping("/analysis")
    public List<Map<String,Integer>> getBookAnalysis(int bookid) {
	return ResolverArticleExecutor.newInstance().all(bookid);
    }
    
    @GetMapping("/analysis_title")
    public List<Map<String,Integer>> getBookAnalysis(String title) {
	int id = DataBaseFascade.newInstance().findBookid(title);
	return ResolverArticleExecutor.newInstance().all(id);
    }
    
}
