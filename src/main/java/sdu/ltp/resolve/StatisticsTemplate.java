package sdu.ltp.resolve;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import sdu.ltp.resolve.AbstractResolver.Node;
import sdu.ltp.resolve.AbstractTreeResolver.TreeNode;
import sdu.ltp.tool.OutputReader;

/**
 * 统计模板
 * @author ljh_2015
 * 
 */
public class StatisticsTemplate {

    /**
     * 统计值
     */
    static final Map<String,Object> templatesValue = new HashMap<>();
    /**
     * 统计节点数
     * @param node
     */
    public static void statistics(TreeNode node) {
	String key = "(0,0,0)";
	putNodeMap(key, node.data.pos);
    }
    
    public static void statisticsNode(Node node) {
	String poskey = "posTable";
	String relkey = "relTable";
	putNodeMap(poskey, node.pos);
	putNodeMap(relkey, node.relate);
    }
    
    public static void showResult() {
	System.out.println("==========TreeNode show==========");
	showTreeNode();
	System.out.println("==========PosTable show==========");
	showPosTable();
	System.out.println("==========RelTable show==========");
	showRelTable();
    }
    
    private static void showTreeNode() {
	showNodeMap("(0,0,0)");
    }
    
    private static void showPosTable() {
	showNodeMap("posTable");
    }
    
    private static void showRelTable() {
	showNodeMap("relTable");
    }
    
    /**
     * 根据key 显示模板
     * @param key
     */
    private static void showNodeMap(String key) {
	@SuppressWarnings("unchecked")
	Map<String,Integer> posValue = (Map<String, Integer>) templatesValue.getOrDefault(key, new TreeMap<String,Integer>());
	sortMap(posValue);
    }
    
    public static void sortMap(Map<String,Integer> maps) {
	List<Entry<String,Integer>> list = new ArrayList<>(maps.entrySet());
	Collections.sort(list,new Comparator<Map.Entry<String, Integer>>() {

	    @Override
	    public int compare(Entry<String, Integer> o1,
		    Entry<String, Integer> o2) {
		return o2.getValue().compareTo(o1.getValue());
	    }
	});
	System.out.println("===============================");
	for(Entry<String,Integer> e:list) {
	    System.out.println(e.getKey()+" : "+e.getValue());
	}
	System.out.println("===============================");
    }
    
    /**
     * 将结果输出到文件中
     * @param file
     * @param maps
     */
    public static void dataOutputFile(File file,Map<String,Integer> maps) {
	List<Entry<String,Integer>> list = new ArrayList<>(maps.entrySet());
	Collections.sort(list,new Comparator<Map.Entry<String, Integer>>() {

	    @Override
	    public int compare(Entry<String, Integer> o1,
		    Entry<String, Integer> o2) {
		return o2.getValue().compareTo(o1.getValue());
	    }
	});
	
	StringBuilder sb = new StringBuilder();
	sb.append("====================================");
	sb.append("\n");
	for(Entry<String,Integer> e:list) {
	    sb.append(e.getKey()+" : "+e.getValue());
	    sb.append("\n");
	}
	sb.append("====================================");
	OutputReader.fileBuilderPrint(file, sb);
    }
    
    
    
    /**
     * 基于map类型的数据统计
     * @param key 参数key
     * @Param property 根据属性统计
     * 
     */
    private static void putNodeMap(String key,String property) {
	@SuppressWarnings("unchecked")
	Map<String,Integer> table = (Map<String, Integer>) templatesValue.getOrDefault(key, new TreeMap<String,Integer>());
	int value = table.getOrDefault(property, 0);
	table.put(property, value+1);
	templatesValue.put(key, table);
    }
}
