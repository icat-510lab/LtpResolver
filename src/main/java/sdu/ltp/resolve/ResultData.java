package sdu.ltp.resolve;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sdu.ltp.entity.Convention;
import sdu.ltp.entity.ConventionHandle;
import sdu.ltp.entity.StyleMapper;

public class ResultData {

    private Map<String,Integer> rel_models = new HashMap<>();
    
    public ResultData() {
	init();
    }
    
    /**
     * 初始化
     */
    public void init() {
	
    }
    
    public void addData(List<StyleMapper> mappers) {
	for(StyleMapper mapper:mappers) {
	    String rel = Convention.decladding(mapper.getRel_lates());
	    Convention.conv_grouping(rel, mapper.getSentid(), new ConventionHandle(){

		@Override
		public void doConvention(long sentid, String key, String value) {
		    int val = rel_models.getOrDefault(key, 0);
		    rel_models.put(key, ++val);
		}
		
	    });
	}
    }
    
    public void addData(ResultData data) {
	Map<String,Integer> datas = data.getRel_models();
	for(String key:datas.keySet()) {
	    int val = datas.get(key);
	    int mval = rel_models.getOrDefault(key, 0);
	    rel_models.put(key, val+mval);
	}
    }
    
    public void addDatas(List<ResultData> datas) {
	for(ResultData data:datas) {
	    addData(data);
	}
    }

    public Map<String, Integer> getRel_models() {
        return rel_models;
    }
    
    
}
