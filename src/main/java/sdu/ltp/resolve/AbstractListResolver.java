package sdu.ltp.resolve;

public abstract class AbstractListResolver extends AbstractResolver {

    class ListNode {

	/**
	 * 层级关系
	 */
	int deep;
	
	Node data;
	
	public ListNode(int deep,Node data) {
	    this.deep = deep;
	    this.data = data;
	}
    }
    
    public abstract void addNode(ListNode node);
}
