package sdu.ltp.resolve;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import sdu.ltp.entity.DataBaseFascade;
import sdu.ltp.entity.ECode;
import sdu.ltp.entity.StyleInstance;
import sdu.ltp.entity.StyleMapper;
import sdu.ltp.resolve.AbstractResolver.Node;

/**
 * 样本解析执行器
 * @author ljh_2015
 *
 */
public final class ResolverSampleExecutor {

    private static final AbstractResolver resolver = new TreeResolver();

    private static final DataBaseFascade dbfascade = new DataBaseFascade();
    
    /**
     * 单句简单解析
     * @param _input_text
     * @return
     */
    public static List<StyleMapper> simpleExecutor(String _input_text) {
	try {
	    BaseData data = BaseDataFactory.getBaseData(_input_text);
	    if(data==null) {
		return null;
	    }
	    //搜索文本库
	    List<Integer> list = dbfascade.queryStorage(_input_text);
	    //若文本库为空，则录入
	    if(list.isEmpty()) {
		dbfascade.TextStorage(_input_text);
		list = dbfascade.queryStorage(_input_text);
	    }
	    //进行解析
	    Map<String,List<Node>> nodes = resolver.resolveData(data);
	    //如果解析数据未被录入，则录入进数据库
	    List<StyleMapper> mappers = new ArrayList<>();
	    for(Integer id:list) {
		for(String key:nodes.keySet()) {
		    StyleInstance instance = new StyleInstance(nodes.get(key),id,key);
		    StyleMapper mapper = instance.transforMapper();
		    mapper.setMap_code(ECode.getDefaultCodeId(mapper.getRel_lates()));
		    mappers.add(mapper);
		    //如果该文本未被作映射操作，则将其映射关系录入库中
		    if(!dbfascade.isMapped(id)) {
			dbfascade.insertMapper(mapper);
		    }
		}
		dbfascade.update(id);
	    }
	    return mappers;
	} catch (Exception e) {
	    e.printStackTrace();
	}
	return null;
    }

    public static List<StyleMapper> MutiExecutor(List<String> _input_group) {
	List<StyleMapper> result = new ArrayList<>();
	for(String text:_input_group) {
	    result.addAll(simpleExecutor(text));
	}
	return result;
    }

}
