package sdu.ltp.resolve;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 原始数据类
 * @author ljh_2015
 * 
 * @param <D>
 *
 */
public class BaseData {

    private String text;
    
    private String body;

    /**
     * 默认构造方法
     */
    public BaseData() {
    }
    
    public BaseData(String text, String body) {
	this.text = text;
	this.body = body;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public InputStream getBody() {
	InputStream in = new ByteArrayInputStream(body.getBytes());
        return in;
    }

    public void setBody(String body) {
        this.body = body;
    }
    
}
