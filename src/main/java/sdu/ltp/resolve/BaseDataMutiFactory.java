package sdu.ltp.resolve;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class BaseDataMutiFactory {

    private final HttpClient client = new HttpClient();
    private final String HTTP_CLIENT = "http:";
    private final String HOST_IP="192.168.9.36";
    private final int port = 8080;
    private final String big_article_url = "http://192.168.9.36:8080/ltp/bigarticle";
    private BlockingQueue<BaseData> queue = null;

    /**
     * 构造函数需要队列
     * @param queue
     */
    public BaseDataMutiFactory(BlockingQueue<BaseData> queue) {
	this.queue = queue;
    }

    /**
     * 获取api链接
     * @param end
     * @param params
     * @param enc
     * @return
     * @throws UnsupportedEncodingException
     */
    public final String getMethodUrl(String end, Map<String,Object> params,String enc) throws UnsupportedEncodingException {
	return HTTP_CLIENT+"//"+HOST_IP+":"+port+end+"?"+getParamsUrl(params, enc);
    }

    /**
     * 获取参数
     * @param params
     * @param enc
     * @return
     * @throws UnsupportedEncodingException
     */
    public final String getParamsUrl(Map<String,Object> params,String enc) throws UnsupportedEncodingException {
	StringBuilder sb = new StringBuilder();
	for(String key:params.keySet()) {
	    sb.append("&");
	    sb.append(key+"="+URLEncoder.encode(params.get(key).toString(), enc));
	}
	return sb.toString().substring(1);
    }

    /**
     * 创建baseData 输送往队列
     * @param title
     * @param body
     */
    public final void createBaseData(String title,String body) {
	if(title==null)
	    return ;
	if(body==null) 
	    return ;
	try {
	    queue.put(new BaseData(title,body));
	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public final void getBaseDatas(int bookid,String title,int sort) throws UnsupportedEncodingException {

	String params = "bookid="+bookid+"&title="+URLEncoder.encode(title,"utf-8")+"&sort="+sort;
	GetMethod get = new GetMethod(big_article_url+"?"+params);
	try {
	    int statuscode = client.executeMethod(get);
	    if(statuscode == HttpStatus.SC_OK)  {
		byte[] m = get.getResponseBody();
		if(m.length==0) {
		    System.out.println("没有库存");
		} else {
		    String result = new String(m,"utf-8");
		    JSONArray array = JSONArray.fromObject(result);
		    for(int i=0;i<array.size();i++) {
			JSONObject obj = array.getJSONObject(i);
			String text = obj.getString("text");
			String body = obj.getString("body");
			//传入队列
			createBaseData(text, body);
		    }
		}
	    }
	} catch (HttpException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	} 
    }

    public BaseData pick() throws InterruptedException {
	return queue.poll(10, TimeUnit.MILLISECONDS);
    }
    
}
