package sdu.ltp.resolve;

import java.net.URLEncoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;

import net.sf.json.JSONObject;

/**
 * 原始数据工厂
 * 生成数据
 * @author ljh_2015
 *
 */
public class BaseDataFactory {

    private static final HttpClient client = new HttpClient();
   
    //private static final String inputPrefix = "http://192.168.9.36:8080/ltp/input?text=";
    private static final String getPrefix = "http://192.168.9.36:8080/ltp/keyeq?text=";

    public static BaseData getBaseData(String text) throws Exception {
	String getUrl = getPrefix+"?text="+URLEncoder.encode(text,"utf-8");
	GetMethod get = new GetMethod(getUrl);
	int statuscode = client.executeMethod(get);
	if(statuscode == HttpStatus.SC_OK) {
	    byte[] m = get.getResponseBody();
	    if(m.length==0) {
		//未录入数据库
		System.out.println("数据库中不存在:"+text);
	    } else {
		String result = new String(m,"utf-8");
		JSONObject obj = JSONObject.fromObject(result);
		String txt = obj.getString("text");
		String body = obj.getString("body");
		return txt!=null & body!=null ? new BaseData(txt,body) : null;
	    }
	}
	
	return null;
    }

}
