package sdu.ltp.tool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 文件输入封装工具类
 * @author ljh_2015
 *
 */
public class InputReader {

    public static Map<String,Integer> readIOToBookMap(int bookid,String chapter_title) {
	String fileName = FileBase.Base_Path+"bookid_"+bookid+"_"+chapter_title;
	BufferedReader reader = null;
	Map<String,Integer> result = new HashMap<>();
	try {
	    reader = new BufferedReader(new FileReader(new File(fileName)));
	    String temp = null;
	    while((temp=reader.readLine())!=null) {
		if(!temp.startsWith("====")) {
		    String[] array = temp.split(" : ");
		    result.put(array[0], Integer.valueOf(array[1]));
		}
	    }
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	} catch (IOException e) {
	    e.printStackTrace();
	}
	return result;
    }
    
}
