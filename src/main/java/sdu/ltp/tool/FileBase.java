package sdu.ltp.tool;

/**
 * 文件基本配置类
 * @author ljh_2015
 *
 */
public class FileBase {
    
    public final static String ROOT = System.getProperty("user.dir");
    public final static String Base_Path = ROOT+"/data/book/analysis/";

}
