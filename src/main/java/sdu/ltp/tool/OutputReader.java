package sdu.ltp.tool;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map.Entry;

/**
 * 文件输出工具类
 * 
 * @author ljh_2015
 *
 */
public class OutputReader {

    public static void filePrint(File file,List<Entry<String, Integer>> list) {
 	FileOutputStream fos = null;
 	try {
 	    fos = new FileOutputStream(file);
 	    for(Entry<String,Integer> mapping:list) {
 		String content = mapping.getKey()+" "+mapping.getValue()+"\n";
 		try {
 		    fos.write(content.getBytes());
 		} catch (IOException e) {
 		    e.printStackTrace();
 		}
 	    }
 	} catch (FileNotFoundException e) {
 	    e.printStackTrace();
 	} finally {
 	    try {
 		if(fos!=null)
 		    fos.close();
 	    } catch (IOException e) {
 		e.printStackTrace();
 	    }
 	}
     }
    
    public static void fileBuilderPrint(File file,StringBuilder build) {
 	FileOutputStream fos = null;
 	if(build==null)
 	    return;
 	try {
 	    fos = new FileOutputStream(file);
 	    try {
		fos.write(build.toString().getBytes());
	    } catch (IOException e) {
		e.printStackTrace();
	    }
 	} catch (FileNotFoundException e) {
 	    e.printStackTrace();
 	} finally {
 	    try {
 		if(fos!=null)
 		    fos.close();
 	    } catch (IOException e) {
 		e.printStackTrace();
 	    }
 	}
     }
    
    public static File createDirFile(String path) {
	File file = new File(path);
	if(file.exists()) {
	    File parent = file.getParentFile();
	    if(!parent.exists()) {
		parent.mkdirs();
	    }
	}
	return file;
    }
    
}
