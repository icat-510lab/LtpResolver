package sdu.ltp.entity;

/**
 * 默认规约接口
 * @author ljh_2015
 *
 */
public class ConventionDefaultHandle implements ConventionHandle {

    private DataBaseFascade fascade = null;
    
    @Override
    public void doConvention(long sentid,String key, String value) {
	if(fascade==null) {
	    fascade = DataBaseFascade.newInstance();
	}
	fascade.setConventionUpdate(sentid, key, value);
    }

}
