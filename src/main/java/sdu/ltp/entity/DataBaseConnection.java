package sdu.ltp.entity;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * 数据库连接
 * @author ljh_2015
 *
 */
public class DataBaseConnection {

    private final static String DRIVER = "com.mysql.jdbc.Driver";
    private final static String URL = "jdbc:mysql://192.168.9.66:3306/segment?autoReconnect=true&characterEncoding=utf-8";
    private final static String USER = "root";
    private final static String PASSWORD = "root";
    private final static int TIMEOUT = 1000*60*60;
    
    /**
     * 与数据库建立连接，未池化处理
     * @return
     */
    public static Connection getConnection() {
	try {
	    Class.forName(DRIVER);
	    Connection conn = DriverManager.getConnection(URL,USER,PASSWORD);
	    return conn;
	} catch (ClassNotFoundException e) {
	    e.printStackTrace();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
	return null;
    }

    /**
     * 验证
     * @param conn
     * @return
     */
    public static boolean isUnException(Connection conn) {
	try {
	    if(!conn.isClosed()) {
		return true;
	    }
	    return conn.isValid(TIMEOUT);
	} catch (SQLException e) {
	    return false;
	} 
    }

}
