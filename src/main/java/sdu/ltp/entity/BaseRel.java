package sdu.ltp.entity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import sdu.ltp.tool.OutputReader;

/**
 * 基本依赖关系
 * 用以编码
 * 目前尚没有用
 * @author ljh_2015
 *
 */
public class BaseRel {

    private final static String[] baseRel = {
	    "SBV","VOB","ADV","ATT",
	    "CMP","COO","POB","WP",
	    "RAD","HED","LAD","DBL",
	    "FOB","IOB"};
    /**
     * 动态数组
     */
    private static ConcurrentMap<String,Integer> automic_rel = new ConcurrentHashMap<String,Integer>();

    static {
	InitLoading();
	for(int i=0;i<baseRel.length;i++) {
	    putKey(baseRel[i]);
	}
    }

    public static void putKey(String key) {
	int value = automic_rel.getOrDefault(key, 0);
	automic_rel.put(key, ++value);
    }
    
    public static void putKeyAndValue(String key,int value) {
	automic_rel.put(key, value);
    }

    public static List<String> getSortedResultSet() {
	List<Entry<String, Integer>> list = new ArrayList<Map.Entry<String,Integer>>(automic_rel.entrySet());
	Collections.sort(list,new Comparator<Entry<String, Integer>>() {

	    @Override
	    public int compare(Entry<String, Integer> o1,
		    Entry<String, Integer> o2) {
		return o1.getValue()>o2.getValue()?-1:1;
	    }
	});
	//打印
	filePrint(list);
	List<String> resultSet = new ArrayList<>();
	for(Entry<String,Integer> mapping:list) {
	    resultSet.add(mapping.getKey());
	}
	return resultSet;
    }

    private static void filePrint(List<Entry<String, Integer>> list) {
	File file = new File("style.rel");
	OutputReader.filePrint(file, list);
    }
    
    private static void InitLoading() {
	File file = new File("style.rel");
	BufferedReader br = null;
	try {
	    br = new BufferedReader(new FileReader(file));
	    String temp = br.readLine();
	    while(temp!=null) {
		String[] args = temp.split(" ");
		String key = args[0];
		int value = Integer.valueOf(args[1]);
		putKeyAndValue(key, value);
		temp = br.readLine();
	    }
	} catch (FileNotFoundException e) {
	    return;
	} catch (IOException e) {
	    e.printStackTrace();
	} finally {
	    if(br!=null) {
		try {
		    br.close();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	    }
	}
    }

}
