package sdu.ltp.entity;

import java.util.Arrays;
import java.util.List;

/**
 * 编码元素
 * @author ljh_2015
 *
 */
public final class ECode {

    private static final char[] codes = {'1','2','3','4','5',
	    '6','7','8','9',
	    'A','B','C','D','E','F','G',
	    'H','I','J','K','L','M','N',
	    'O','P','Q','R','S','T','U','V','W','X','Y','Z'};
    public static final char getCode(int i,boolean cae) {
	if(cae) {
	    return (char) (codes[i]+32);
	}
	return codes[i];
    }
     
    public static final String getCodeId(List<String> cur,List<String> mother,boolean cae) {
	StringBuilder build = new StringBuilder();
	if(mother.size()>codes.length) {
	    throw new ECodeOutLengthException("the array of mother code is larger than the number of code element");
	}
	for(String element:cur) {
	    BaseRel.putKey(element);
	    if(mother.contains(element)) {
		build.append(getCode(mother.indexOf(element),cae));
	    } else
		throw new ECodeNoElementException("this element "+element+" is not in the init group");
	}
	return build.toString();
    }
    
    public static final String getDefaultCodeId(String rel) {
	return getCodeId(resolve(rel), BaseRel.getSortedResultSet(), false);
    }
    
    /**
     * @param rel SBV_ADV_ADV
     * @return
     */
    private static final List<String> resolve(String rel) {
	int len = rel.length()-1;
	rel = rel.substring(1,len);
	return Arrays.asList(rel.split("_"));
    }
    
}