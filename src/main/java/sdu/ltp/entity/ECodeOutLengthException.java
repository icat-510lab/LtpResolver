package sdu.ltp.entity;

public class ECodeOutLengthException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -2153336916841179212L;
    private String message;

    public ECodeOutLengthException(String message) {
	this.message = message;
    }
    
    @Override
    public String getMessage() {
        return message;
    }
    
}
