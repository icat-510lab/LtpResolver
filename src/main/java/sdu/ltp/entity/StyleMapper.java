package sdu.ltp.entity;

/**
 * 句式映射类
 * @author ljh_2015
 *
 */
public class StyleMapper {

    //句子Id
    private long sentid;
    
    //映射
    private String sort_key;
    
    private String rel_lates;
    
    private String map_code;
    
    private String pos_lates;
    
    private String word_lates;

    public static StyleMapper newInstance() {
	return new StyleMapper();
    }
    
    public long getSentid() {
        return sentid;
    }

    public void setSentid(long sentid) {
        this.sentid = sentid;
    }

    public String getSort_key() {
        return sort_key;
    }

    public void setSort_key(String sort_key) {
        this.sort_key = sort_key;
    }

    public String getRel_lates() {
        return rel_lates;
    }

    public void setRel_lates(String rel_lates) {
        this.rel_lates = rel_lates;
    }

    public String getPos_lates() {
        return pos_lates;
    }

    public void setPos_lates(String pos_lates) {
        this.pos_lates = pos_lates;
    }

    public String getWord_lates() {
        return word_lates;
    }

    public void setWord_lates(String word_lates) {
        this.word_lates = word_lates;
    }
    
    public void setMap_code(String map_code) {
        this.map_code = map_code;
    }

    public String getMap_code() {
        return this.map_code;
    }
    
    
    
    
    
}
