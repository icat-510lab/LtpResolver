package sdu.ltp.entity;

/**
 * 规约实例化
 * @author ljh_2015
 *
 */
public interface ConventionHandle {

    /**
     * 规约
     * 存储管理
     * @param key
     * @param value
     */
    void doConvention(long sentid,String key,String value);
    
}
