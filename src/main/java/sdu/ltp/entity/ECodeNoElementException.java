package sdu.ltp.entity;

public class ECodeNoElementException extends RuntimeException {

    /**
     * 
     */
    private static final long serialVersionUID = -3256898334196001476L;

    private String message;

    public ECodeNoElementException(String message) {
	this.message = message;
    }
    
    @Override
    public String getMessage() {
        return message;
    }
    
}
