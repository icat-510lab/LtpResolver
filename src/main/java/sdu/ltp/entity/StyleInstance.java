package sdu.ltp.entity;

import java.util.List;

import sdu.ltp.resolve.AbstractResolver.Node;

/**
 * 句法样式实例
 * @author ljh_2015
 *
 */
public class StyleInstance {

    private List<Node> nodes;
    
    private int sentid;
    
    private String key;
    
    /**
     * 根据解析的节点获取句法主干样式，并封装成类
     * @param nodes
     * @param sentid
     * @param key
     */
    public StyleInstance(List<Node> nodes,int sentid,String key) {
	this.nodes = nodes;
	this.sentid = sentid;
	this.key = key;
    }
    
    /**
     * 词性输出
     * @return
     */
    private String posValue() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	for(int i=0;i<nodes.size();i++) {
	    if(i!=0) {
		sb.append("_");
	    } 
	    sb.append(nodes.get(i).getPos());
	}
	sb.append("]");
	return sb.toString();
    }
    
    /**
     * 依赖关系输出
     * @return
     */
    private String relValue() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	for(int i=0;i<nodes.size();i++) {
	    if(i!=0) {
		sb.append("_");
	    } 
	    sb.append(nodes.get(i).getRelate());
	}
	sb.append("]");
	return sb.toString();
    }
    
    /**
     * 词语输出
     * @return
     */
    private String wordValue() {
	StringBuilder sb = new StringBuilder();
	sb.append("[");
	for(int i=0;i<nodes.size();i++) {
	    if(i!=0) {
		sb.append("_");
	    } 
	    sb.append(nodes.get(i).getText());
	}
	sb.append("]");
	return sb.toString();
    }
    
    /**
     * 转换为styleMapper类;
     * @return
     */
    public StyleMapper transforMapper() {
	StyleMapper mapper = StyleMapper.newInstance();
	mapper.setPos_lates(posValue());
	mapper.setRel_lates(relValue());
	mapper.setWord_lates(wordValue());
	mapper.setSentid(sentid);
	mapper.setSort_key(key);
	return mapper;
    }
    
}
